from uszipcode import SearchEngine
from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import pandas as pd

# load data from csv file
df = pd.read_csv('train.csv')
df_test = pd.read_csv('test.csv')

# split data into input and target
# columns:
# yellow (general policy data):
# InceptionDate,Type,InsuredName,InsuredState,BrokerCompany,BrokerContact,BrokerState,UnderwriterTeam,BusinessClassification
# blue (specific policy data):
# Occurrence: General Liability,Occurrence: Owners & Contractors Protective,Claims Made: Products Liability,Claims Made: Life Sciences Liability,Excess Claims Made: Products Liability,Excess Claims Made: Life Sciences Liability,Limit Damage to Premises Rented to You,Each Common Cause Liquor Liability,Limit Products / Completed Operations Aggregate,Per Occurrence Coverage Limits,General Aggregate Coverage Limits,Limit Personal / Advertising Injury,Limit Medical Expense,Limit Per Project Aggregate,Each Employee Employee Benefits Liability,Limit Per Location Aggregate,Employee Benefits Liability,Assault & Battery,Hired / Non-Owned Auto Liability,Liquor Liability,Data Breach,Other,Per Claim Limit Coverage Limits,Coverage Premium,Terrorism
# grey (annex, if 1 in use, if 0 not in use):
# MJIL 1000 08 10,MPIL 1007 03 14,MPIL 1083 04 15,MDIL 1000 08 11,MDIL 1001 08 11,IL 00 17 11 98,IL 00 21 09 08,MEIL 1200 10 16,MEIL 1225 10 11,MEIL 1231 10 13,MDGL 1000 01 13,MDGL 1008 08 11,CG 00 01 04 13,CG 00 33 04 13,CG 20 15 04 13,CG 21 35 10 01,CG 21 47 12 07,CG 21 49 09 99,CG 21 73 01 15,CG 21 96 03 05,ME 037 04 99,MEGL 0008 01 16,MEGL 0048 03 13,MEGL 0170 05 16,MEGL 0219 05 16,MEGL 1394 05 16,MEGL 1671 05 16,MEGL 1674 05 16,MEGL 1884 10 15,MEGL 5300 05 16,MEGL 5302 05 16,MEGL 5303 05 16,CG 21 86 12 04,MEIL 1211 06 10,MDGL 1012 01 13,CG 00 09 04 13,CG 29 60 06 15,MEGL 0001-OCP 08 14,MEGL 1273 07 16,MEGL 1637 05 17,MPIL 1009-TX 01 16,MPIL 1010-TX 05 10,CG 20 01 04 13,CG 20 10 04 13,CG 20 37 04 13,CG 21 36 03 05,CG 22 79 04 13,MEGL 0241-01 05 16,MEGL 0313 02 17,MEGL 1331 11 15,MEGL 1349 05 17,MEGL 1361 05 16,MEGL 1623 08 17,MEGL 1625 11 13,MEGL 1673 05 16,MEGL 1681 07 16,MEGL 1849 08 14,MEGL 1897 02 16,MGL 1319 01 16,CG 04 35 12 07,CG 20 18 04 13,CG 21 55 09 99,MEGL 1879 07 15,MEIL 1200-CA 03 16,MEGL 5301 05 16,MEIL 1241 07 14,MEIL 1309 11 13,CG 21 65 12 04,MEGL 1895 02 16,MPIL 1005-CA 01 17,CG 00 02 04 13,CG 00 38 04 13,CG 21 16 04 13,CG 21 39 10 93,MEGL 1251 05 16,PD-12000-01 03 04,EIC 701-02 08 07,PD-22000-02 06 04,EIC 4115-01 2 03,MEGL 1660 05 15,ZZ-44003-03 01 15,MPIL 1041 02 12,MDGL 1008 08 11 BC,CG 20 28 04 13,MDIL 1005 08 14,MEGL 1624 05 16,CG 20 11 04 13,MEGL 1378 05 16,CG 24 10 07 98,MEGL 1680 07 16,MEGL 1889 10 15,CG 20 10 07 04,CG 20 37 07 04,CG 20 26 04 13,CG 24 26 04 13,MPIL 1006-FL 01 10,CG 24 04 05 09,CG 21 33 11 85,CG 21 53 01 96,MEGL 1333 03 14,MEGL 1892 12 15,IL 12 01 11 85,CG 22 50 04 13,MEGL 1397 07 10,CG 02 05 12 04,CG 22 43 04 13,MEGL 0217 11 16,MEGL 0026 05 16,MEGL 2202 01 17,MEIL 1247 08 15,MIL 1214 09 17

# yellow and blue are inputs
# grey is output

# define neural network architecture
X_train = df[['BusinessClassification', 'Occurrence: General Liability', 'Occurrence: Owners & Contractors Protective', 'Claims Made: Products Liability', 'Claims Made: Life Sciences Liability', 'Excess Claims Made: Products Liability', 'Excess Claims Made: Life Sciences Liability', 'Limit Damage to Premises Rented to You', 'Each Common Cause Liquor Liability', 'Limit Products / Completed Operations Aggregate', 'Per Occurrence Coverage Limits',
              'General Aggregate Coverage Limits', 'Limit Personal / Advertising Injury', 'Limit Medical Expense', 'Limit Per Project Aggregate', 'Each Employee Employee Benefits Liability', 'Limit Per Location Aggregate', 'Employee Benefits Liability', 'Assault & Battery', 'Hired / Non-Owned Auto Liability', 'Liquor Liability', 'Data Breach', 'Other', 'Per Claim Limit Coverage Limits', 'Coverage Premium', 'Terrorism']]

y_train = df[['MJIL 1000 08 10', 'MPIL 1007 03 14', 'MPIL 1083 04 15', 'MDIL 1000 08 11', 'MDIL 1001 08 11', 'IL 00 17 11 98', 'IL 00 21 09 08', 'MEIL 1200 10 16', 'MEIL 1225 10 11', 'MEIL 1231 10 13', 'MDGL 1000 01 13', 'MDGL 1008 08 11', 'CG 00 01 04 13', 'CG 00 33 04 13', 'CG 20 15 04 13', 'CG 21 35 10 01', 'CG 21 47 12 07', 'CG 21 49 09 99', 'CG 21 73 01 15', 'CG 21 96 03 05', 'ME 037 04 99', 'MEGL 0008 01 16', 'MEGL 0048 03 13', 'MEGL 0170 05 16', 'MEGL 0219 05 16', 'MEGL 1394 05 16', 'MEGL 1671 05 16', 'MEGL 1674 05 16', 'MEGL 1884 10 15', 'MEGL 5300 05 16', 'MEGL 5302 05 16', 'MEGL 5303 05 16', 'CG 21 86 12 04', 'MEIL 1211 06 10', 'MDGL 1012 01 13', 'CG 00 09 04 13', 'CG 29 60 06 15', 'MEGL 0001-OCP 08 14', 'MEGL 1273 07 16', 'MEGL 1637 05 17', 'MPIL 1009-TX 01 16', 'MPIL 1010-TX 05 10', 'CG 20 01 04 13', 'CG 20 10 04 13', 'CG 20 37 04 13', 'CG 21 36 03 05', 'CG 22 79 04 13', 'MEGL 0241-01 05 16', 'MEGL 0313 02 17', 'MEGL 1331 11 15', 'MEGL 1349 05 17', 'MEGL 1361 05 16', 'MEGL 1623 08 17', 'MEGL 1625 11 13', 'MEGL 1673 05 16',
              'MEGL 1681 07 16', 'MEGL 1849 08 14', 'MEGL 1897 02 16', 'MGL 1319 01 16', 'CG 04 35 12 07', 'CG 20 18 04 13', 'CG 21 55 09 99', 'MEGL 1879 07 15', 'MEIL 1200-CA 03 16', 'MEGL 5301 05 16', 'MEIL 1241 07 14', 'MEIL 1309 11 13', 'CG 21 65 12 04', 'MEGL 1895 02 16', 'MPIL 1005-CA 01 17', 'CG 00 02 04 13', 'CG 00 38 04 13', 'CG 21 16 04 13', 'CG 21 39 10 93', 'MEGL 1251 05 16', 'PD-12000-01 03 04', 'EIC 701-02 08 07', 'PD-22000-02 06 04', 'EIC 4115-01 2 03', 'MEGL 1660 05 15', 'ZZ-44003-03 01 15', 'MPIL 1041 02 12', 'MDGL 1008 08 11 BC', 'CG 20 28 04 13', 'MDIL 1005 08 14', 'MEGL 1624 05 16', 'CG 20 11 04 13', 'MEGL 1378 05 16', 'CG 24 10 07 98', 'MEGL 1680 07 16', 'MEGL 1889 10 15', 'CG 20 10 07 04', 'CG 20 37 07 04', 'CG 20 26 04 13', 'CG 24 26 04 13', 'MPIL 1006-FL 01 10', 'CG 24 04 05 09', 'CG 21 33 11 85', 'CG 21 53 01 96', 'MEGL 1333 03 14', 'MEGL 1892 12 15', 'IL 12 01 11 85', 'CG 22 50 04 13', 'MEGL 1397 07 10', 'CG 02 05 12 04', 'CG 22 43 04 13', 'MEGL 0217 11 16', 'MEGL 0026 05 16', 'MEGL 2202 01 17', 'MEIL 1247 08 15', 'MIL 1214 09 17']]

print(X_train.shape)
# print(X_train[:5])
print(y_train.shape)
# print(y_train[:5])


X_test = df_test[['BusinessClassification', 'Occurrence: General Liability', 'Occurrence: Owners & Contractors Protective', 'Claims Made: Products Liability', 'Claims Made: Life Sciences Liability', 'Excess Claims Made: Products Liability', 'Excess Claims Made: Life Sciences Liability', 'Limit Damage to Premises Rented to You', 'Each Common Cause Liquor Liability', 'Limit Products / Completed Operations Aggregate', 'Per Occurrence Coverage Limits',
                  'General Aggregate Coverage Limits', 'Limit Personal / Advertising Injury', 'Limit Medical Expense', 'Limit Per Project Aggregate', 'Each Employee Employee Benefits Liability', 'Limit Per Location Aggregate', 'Employee Benefits Liability', 'Assault & Battery', 'Hired / Non-Owned Auto Liability', 'Liquor Liability', 'Data Breach', 'Other', 'Per Claim Limit Coverage Limits', 'Coverage Premium', 'Terrorism']]

# map 'InsuredState' and 'BusinessClassification' to numerical values
# and normalize
# sr = SearchEngine()
# result = sr.by_state(str(X_train['InsuredState']))
# if (result.__len__ > 0):
#     X_train['InsuredState'] = abs(result.lat) + abs(result.lng)
# else:
#     X_train['InsuredState'] = 0
X_train['BusinessClassification'] = str(
    X_train['BusinessClassification']).split(" ")[0]

# result = sr.by_state(str(X_test['InsuredState']))
# if (result.__len__ > 0):
#     X_test['InsuredState'] = abs(result.lat) + abs(result.lng)
# else:
#     X_test['InsuredState'] = 0
X_test['BusinessClassification'] = str(
    X_test['BusinessClassification']).split(" ")[0]


scaler = MinMaxScaler()

# define the column(s) to normalize
cols_to_normalize = ['BusinessClassification']

# normalize the column(s)
X_train[cols_to_normalize] = scaler.fit_transform(X_train[cols_to_normalize])
X_test[cols_to_normalize] = scaler.fit_transform(X_test[cols_to_normalize])

y_test = df_test[['MJIL 1000 08 10', 'MPIL 1007 03 14', 'MPIL 1083 04 15', 'MDIL 1000 08 11', 'MDIL 1001 08 11', 'IL 00 17 11 98', 'IL 00 21 09 08', 'MEIL 1200 10 16', 'MEIL 1225 10 11', 'MEIL 1231 10 13', 'MDGL 1000 01 13', 'MDGL 1008 08 11', 'CG 00 01 04 13', 'CG 00 33 04 13', 'CG 20 15 04 13', 'CG 21 35 10 01', 'CG 21 47 12 07', 'CG 21 49 09 99', 'CG 21 73 01 15', 'CG 21 96 03 05', 'ME 037 04 99', 'MEGL 0008 01 16', 'MEGL 0048 03 13', 'MEGL 0170 05 16', 'MEGL 0219 05 16', 'MEGL 1394 05 16', 'MEGL 1671 05 16', 'MEGL 1674 05 16', 'MEGL 1884 10 15', 'MEGL 5300 05 16', 'MEGL 5302 05 16', 'MEGL 5303 05 16', 'CG 21 86 12 04', 'MEIL 1211 06 10', 'MDGL 1012 01 13', 'CG 00 09 04 13', 'CG 29 60 06 15', 'MEGL 0001-OCP 08 14', 'MEGL 1273 07 16', 'MEGL 1637 05 17', 'MPIL 1009-TX 01 16', 'MPIL 1010-TX 05 10', 'CG 20 01 04 13', 'CG 20 10 04 13', 'CG 20 37 04 13', 'CG 21 36 03 05', 'CG 22 79 04 13', 'MEGL 0241-01 05 16', 'MEGL 0313 02 17', 'MEGL 1331 11 15', 'MEGL 1349 05 17', 'MEGL 1361 05 16', 'MEGL 1623 08 17', 'MEGL 1625 11 13', 'MEGL 1673 05 16',
                  'MEGL 1681 07 16', 'MEGL 1849 08 14', 'MEGL 1897 02 16', 'MGL 1319 01 16', 'CG 04 35 12 07', 'CG 20 18 04 13', 'CG 21 55 09 99', 'MEGL 1879 07 15', 'MEIL 1200-CA 03 16', 'MEGL 5301 05 16', 'MEIL 1241 07 14', 'MEIL 1309 11 13', 'CG 21 65 12 04', 'MEGL 1895 02 16', 'MPIL 1005-CA 01 17', 'CG 00 02 04 13', 'CG 00 38 04 13', 'CG 21 16 04 13', 'CG 21 39 10 93', 'MEGL 1251 05 16', 'PD-12000-01 03 04', 'EIC 701-02 08 07', 'PD-22000-02 06 04', 'EIC 4115-01 2 03', 'MEGL 1660 05 15', 'ZZ-44003-03 01 15', 'MPIL 1041 02 12', 'MDGL 1008 08 11 BC', 'CG 20 28 04 13', 'MDIL 1005 08 14', 'MEGL 1624 05 16', 'CG 20 11 04 13', 'MEGL 1378 05 16', 'CG 24 10 07 98', 'MEGL 1680 07 16', 'MEGL 1889 10 15', 'CG 20 10 07 04', 'CG 20 37 07 04', 'CG 20 26 04 13', 'CG 24 26 04 13', 'MPIL 1006-FL 01 10', 'CG 24 04 05 09', 'CG 21 33 11 85', 'CG 21 53 01 96', 'MEGL 1333 03 14', 'MEGL 1892 12 15', 'IL 12 01 11 85', 'CG 22 50 04 13', 'MEGL 1397 07 10', 'CG 02 05 12 04', 'CG 22 43 04 13', 'MEGL 0217 11 16', 'MEGL 0026 05 16', 'MEGL 2202 01 17', 'MEIL 1247 08 15', 'MIL 1214 09 17']]

print(X_test.shape)
# print(X_test[:5])
print(y_test.shape)
# print(Y[:5])


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(26, 300)
        # self.fc2 = nn.Linear(100, 300)
        self.fc3 = nn.Linear(300, 111)

    def forward(self, x):
        x = torch.relu(self.fc1(x))
        # x = torch.relu(self.fc2(x))
        x = self.fc3(x)
        return x


# initialize model, loss function, and optimizer
model = Net()
criterion = nn.MSELoss()
optimizer = optim.SGD(model.parameters(), lr=0.01)

# train model
epoch_range = 10000
for epoch in range(epoch_range):
    # forward pass
    X_array = np.array(X_train, dtype=np.float32)
    X_tensor = torch.tensor(X_array)
    y_pred = model(X_tensor)
    y_array = np.array(y_train, dtype=np.float32)
    y_tensor = torch.tensor(y_array)
    loss = criterion(y_pred, y_tensor)

    # backward pass
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

    if (epoch + 1) % 100 == 0:
        print('Epoch [{}/{}], Loss: {:.4f}'.format(epoch +
              1, epoch_range, loss.item()))


# test model
with torch.no_grad():
    X_array = np.array(X_test, dtype=np.float32)
    X_tensor = torch.tensor(X_array)

    y_pred = model(X_tensor)
    y_array = np.array(y_test, dtype=np.float32)
    y_tensor = torch.tensor(y_array)
    loss = criterion(y_pred, y_tensor)
    print('Test Loss: {:.4f}'.format(loss.item()))

    # print(y_pred)
    # print(y_tensor)

    y_pred = y_pred.numpy()
    y_tensor = y_tensor.numpy()

    print(y_pred.shape)
    print(y_tensor.shape)

    # convert y_pred values if they are less than 0.4 to 0, otherwise 1
    y_pred[y_pred < 0.4] = 0
    y_pred[y_pred >= 0.4] = 1

    print("y_pred")
    print(y_pred[0])
    print("actual")
    print(y_tensor[0])
